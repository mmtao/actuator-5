/* software version of STM32 HAL_CRC_Calculate()
 * https://my.st.com/public/STe2ecommunities/mcu/Lists/cortex_mx_stm32/DispForm.aspx?ID=44842&Source=/public/STe2ecommunities/mcu/Tags.aspx?tags=CRC32
 */

#include <stdint.h>

static uint32_t calculateWord (uint32_t crc, uint32_t data)
{
    crc = crc ^ data;

    int i;
    for (i = 0; i < 32; i++) {
        if ((crc & 0x80000000) != 0) {
            crc = (crc << 1) ^ 0x04C11DB7; // Polynomial used in STM32
        } else {
            crc = (crc << 1);
        }
    }

    return (crc);
}


uint32_t calcSTM32CRC (uint32_t *buffer, uint32_t count)
{
    uint32_t crc = 0xFFFFFFFF;

    while (count-- > 0) {
        crc = calculateWord (crc, *buffer++);
    }

    return (crc);
}
