/* support in-application programming.
 * based on STM32Cube/Repository/STM32Cube_FW_F4_V1.15.0/Projects/STM32446E_EVAL/Applications/IAP/IAP_Main
 */

#include "actuator.h"

#define PROG_START      0x8000000U              // FLASH ORIGIN must match STM32F446ZETx_FLASH.ld 
#define MAX_SIZE        30000                   // max temp program size, 4-byte words

static uint32_t new_prog[MAX_SIZE];             // staging area
static uint16_t prog_i;                         // next index

/* set program address.
 * return whether deemed valid.
 */
bool iap_setAddress (uint32_t a)
{
    if (a < MAX_SIZE) {
        prog_i = a;
        return (true);
    }
    return (false);
}

/* save the given program value and autoincrement address.
 * return whether there is still room.
 */
bool iap_setNextProgram (uint32_t v)
{
    if (prog_i >= MAX_SIZE) {
        return (false);
    }
    new_prog[prog_i++] = v;
    return (true);
}

__attribute__ ((section (".data"))) void toggleLED (int count)
{
    FAST_WR_PIN (LED_GPIO_Port, LED_Pin, GPIO_PIN_RESET);
    for (volatile uint32_t i = 0; i < 3000000UL; i++) {
        continue;
    }
    for (int i = 0; i < count; i++) {
        FAST_WR_PIN (LED_GPIO_Port, LED_Pin, GPIO_PIN_SET);
        for (volatile uint32_t i = 0; i < 500000UL; i++) {
            continue;
        }
        FAST_WR_PIN (LED_GPIO_Port, LED_Pin, GPIO_PIN_RESET);
        for (volatile uint32_t i = 0; i < 1500000UL; i++) {
            continue;
        }
    }
    for (volatile uint32_t i = 0; i < 3000000UL; i++) {
        continue;
    }
}

/* program the FLASH with the new program and reboot.
 * N.B. this function must be in RAM and not call any other functions not also in RAM once the erase begins.
 */
__attribute__ ((section (".data"))) bool iap_commit()
{
    // check basic program integrity
    if (prog_i == 0 || prog_i >= MAX_SIZE || calcSTM32CRC (new_prog, prog_i-1) != new_prog[prog_i-1]) {
        toggleLED(3);
        return (false);
    }

    // unlock FLASH to allow programming
    if (HAL_FLASH_Unlock() != HAL_OK) {
        return (false);
    }

    // Clear pending flags (if any) */
    __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR |
                           FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR | FLASH_FLAG_PGSERR);

    // disable interrupts because their vectors are in the FLASH we will soon erase
    __disable_irq();

    // wait for any previous FLASH function to complete
    while(__HAL_FLASH_GET_FLAG(FLASH_FLAG_BSY) != RESET) {
        continue;
    }

    // light the LED for fun
    FAST_WR_PIN (LED_GPIO_Port, LED_Pin, GPIO_PIN_SET);

    // mass-erase all FLASH -- takes a few seconds so don't worry
    FLASH->CR = FLASH_CR_MER | FLASH_CR_STRT | ((uint32_t)FLASH_VOLTAGE_RANGE_3 <<8U);
    while(__HAL_FLASH_GET_FLAG(FLASH_FLAG_BSY) != RESET) {
        continue;
    }

    // write each word
    for (int i = 0; i < prog_i-1; i++) {                   // don't include CRC in last word
        uint32_t flash_addr = PROG_START + 4U*i;
        uint32_t flash_data = new_prog[i];

        // wait for ready
        while(__HAL_FLASH_GET_FLAG(FLASH_FLAG_BSY) != RESET) {
            continue;
        }

        // Clear FLASH End of Operation pending bit
        if (__HAL_FLASH_GET_FLAG(FLASH_FLAG_EOP) != RESET) {
            __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP);
        }

        // go
        CLEAR_BIT(FLASH->CR, FLASH_CR_PSIZE);
        FLASH->CR |= FLASH_PSIZE_WORD;
        FLASH->CR |= FLASH_CR_PG;
        *(__IO uint32_t*)flash_addr = flash_data;
        FLASH->CR &= (~FLASH_CR_PG);
    }

    // boot new program
    SCB->AIRCR  = ((0x5FAUL << SCB_AIRCR_VECTKEY_Pos) | SCB_AIRCR_SYSRESETREQ_Msk);
    while(1) {
        continue;
    }
}
