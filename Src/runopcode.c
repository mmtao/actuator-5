/* functions and jump table to perform each H2A message command opcode.
 * some may change the current state.
 */


#include "actuator.h"

// record of FeedForward->RunPosition ordering and all opcodes received
static bool ff_before_run;
static uint32_t opcodes_seen_mask;

// special payload cookie value that causes message to be largely ignored
static const uint32_t IGNORE_MSG = 0xDEADDEAF;

/* return whether payload is to be ignored
 */
static bool ignorePayload()
{
    return (!memcmp (&h2a_msg[PAYLOAD_OFFSET], &IGNORE_MSG, PAYLOAD_LEN));
}

/* arrange to acknowledge the given opcode in lieu of the next StatusBits
 */
static void ackOpCode (DM_OpCode opcode)
{
    next_a2h_status = opcode;
}

/* save the h2a_msg payload to the given opcode_values entry
 */
static void captureH2AOpCode (DM_OpCode opcode)
{
    memcpy (&opcode_values[opcode], &h2a_msg[PAYLOAD_OFFSET], PAYLOAD_LEN);
}

/* handle default opcode processing:
 *   copy payload to the persistent array,
 *   note it has been seen in opcodes_seen_mask,
 *   arrange for next a2h StatusBits message to be the same opcode.
 */
static void runOpCode_default (DM_OpCode opcode)
{
    if (!ignorePayload()) {
        captureH2AOpCode (opcode);
        if ((int)opcode < 32) {
            opcodes_seen_mask |= (1 << opcode);
        }
    }

    ackOpCode(opcode);
}

/* handle receipt of new RunPosition opcode
 */
static void runOpCode_RunPosition()
{
    // check if ignore
    if (ignorePayload()) {
        return;    // do not ack
    }

    // insure all prerequisites are met including having seen a feed-forward since we were last called
    if (status_bits & RunPosNotReady_SBM) {
        return;
    }
    if (!ff_before_run) {
        status_bits |= PosWithoutFF_SBM;
        return;
    }

    // reject if candidate position is out of range
    float new_pos;
    memcpy (&new_pos, &h2a_msg[PAYLOAD_OFFSET], PAYLOAD_LEN);
    if (new_pos < opcode_values[MinPosition_OC]) {
        status_bits |= PositionCmdTooLo_SBM;
        return;
    } else if (new_pos > opcode_values[MaxPosition_OC]) {
        status_bits |= PositionCmdTooHi_SBM;
        return;
    }

    // reject if still in coil hold period
    if (getClock() - wildhold_t0 <= WILD_HOLD_CT) {
        return;
    }

    // reset flag to wait for next FeedForward IFF new value is different.
    // this is because we might well get another RP just because some other Actuator is getting set.
    if (opcode_values[RunPosition_OC] != new_pos) {
        ff_before_run = false;
    }

    // ok, commit to new set point
    opcode_values[RunPosition_OC] = new_pos;

    // set flag for sendTuningMessage()
    new_run_pos = true;

    // init PID if not already running
    if ((status_bits & PositionElseCoil_SBM) == 0) {

        // engage positon state
        status_bits |= PositionElseCoil_SBM;

        // initialize the PID controller with new RunPosition value
        initPositionState();
    }

    // N.B. do not ack
}

/* handle receipt of new RunCoil opcode
 */
static void runOpCode_RunCoil()
{
    // check if ignore
    if (ignorePayload()) {
        return;    // do not ack
    }

    // insure all prerequisites are met
    if (status_bits & RunCoilNotReady_SBM) {
        return;
    }

    // check whether candidate coil value is out of range
    float new_coil;
    memcpy (&new_coil, &h2a_msg[PAYLOAD_OFFSET], PAYLOAD_LEN);
    if (new_coil < -1) {
        status_bits |= CoilCmdTooLo_SBM;
        return;
    } else if (new_coil > 1) {
        status_bits |= CoilCmdTooHi_SBM;
        return;
    }

    // ok, commit to new set point
    captureH2AOpCode (RunCoil_OC);

    // revert to coil mode
    status_bits &= ~PositionElseCoil_SBM;

    // N.B. do not ack
}

/* handle receipt of new StartTuning opcode
 */
static void runOpCode_StartTuning()
{
    // check if ignore
    if (ignorePayload()) {
        return;    // do not ack
    }

    // save value
    captureH2AOpCode (StartTuning_OC);

    // update status bit
    switch ((int)opcode_values[StartTuning_OC]) {
    case 0:
        // turn off
        status_bits &= ~TuningActive_SBM;
        break;
    case 1: // fallthru
    case 2:
        // 1 or 2 turns on (1 is normal, 2 is special debug mode)
        status_bits |= TuningActive_SBM;
        // init db toggle to insure fresh message is built
        a2h_db_toggle = NULL;
        break;
    default:
        // anything else ignored
        break;
    }

    // N.B. do not ack
}

/* handle receipt of new FeedForward opcode
 */
static void runOpCode_FeedForward ()
{
    // check if ignore
    if (ignorePayload()) {
        return;    // do not ack
    }

    // note arrival for RunPosition requirement
    ff_before_run = true;

    // save value
    captureH2AOpCode (FeedForward_OC);

    // N.B. do not ack
}

/* handle receipt of any new FIR4 Filter opcode
 */
static void runOpCode_FIR4Filter (FIR4Filter *fp, float *vp, DM_OpCode opcode)
{
    // check if ignore
    if (ignorePayload()) {
        ackOpCode(opcode);
        return;
    }

    // save and note we received payload
    runOpCode_default (opcode);

    // install in working filter at proper location
    *vp = opcode_values[opcode];
}

/* handle receipt of any new FIR7 Filter opcode
 */
static void runOpCode_FIR7Filter (FIR7Filter *fp, float *vp, DM_OpCode opcode)
{
    // check if ignore
    if (ignorePayload()) {
        ackOpCode(opcode);
        return;
    }

    // save and note we received payload
    runOpCode_default (opcode);

    // install in working filter at proper location
    *vp = opcode_values[opcode];
}

/* handle receipt of Temperature opcode
 */
static void runOpCode_Temperature()
{
    // check if ignore
    if (ignorePayload()) {
        ackOpCode(Temperature_OC);
        return;
    }

    // save temperatore
    opcode_values[Temperature_OC] = readTemperature();

    // record this opcode as next response
    ackOpCode(Temperature_OC);
}

/* handle receipt of LEDBlinkRate
 */
static void runOpCode_LEDBlinkRate()
{
    // check if ignore
    if (ignorePayload()) {
        ackOpCode(LEDBlinkRate_OC);
        return;
    }

    // save and note we received payload
    runOpCode_default (LEDBlinkRate_OC);

    // convert rate in Hz to number of rising Go edges for full cycle, beware 0
    if (opcode_values[LEDBlinkRate_OC] > 0) {
        led_count = GO_PER_SEC / opcode_values[LEDBlinkRate_OC];
    } else {
        led_count = 0;
    }

    // reset counter
    led_counter = 0;
}

/* handle receipt of GetOpCode opcode
 * return whether the queried opcode is valid.
 */
static bool runOpCode_GetOpCode ()
{
    // get opcode being queried from the h2a_msg payload
    uint32_t opcode;
    memcpy ((void*)&opcode, &h2a_msg[PAYLOAD_OFFSET], PAYLOAD_LEN);

    // ignore if out of range
    if (opcode >= N_ACTUATOR_OC) {
        return (false);
    }

    // convert to DM_OpCode
    DM_OpCode op = (DM_OpCode) opcode;

    // check if ignore
    if (ignorePayload()) {
        ackOpCode(op);
        return(true);
    }

    // seems friendly to read a fresh temp if that's the opcode being requested
    if (op == Temperature_OC) {
        opcode_values[Temperature_OC] = readTemperature();
    }

    // set the queried opcode as next response
    ackOpCode(op);

    // capture
    captureH2AOpCode (GetOpCode_OC);

    // N.B. ack op, not GetOpCode_OC

    // ok
    return (true);
}

/* handle receipt of EStop
 */
static void runOpCode_EStop()
{
    // force immediate safe current
    forceSafeCoil();

    // echo
    captureH2AOpCode (EStop_OC);
    ackOpCode (EStop_OC);
}

/* handle receipt of ProgramAddr opcode
 */
static void runOpCode_ProgramAddr()
{
    // check if ignore
    if (ignorePayload()) {
        ackOpCode (ProgramAddr_OC);
        return;
    }

    // save value
    captureH2AOpCode (ProgramAddr_OC);

    // set starting program address, record if error
    uint32_t a;
    memcpy (&a, &opcode_values[ProgramAddr_OC], sizeof(a));
    if (!iap_setAddress (a)) {
        status_bits |= FLASHError_SBM;
    }

    // echo
    ackOpCode(ProgramAddr_OC);
}

/* handle receipt of ProgramDataInc opcode
 */
static void runOpCode_ProgramDataInc()
{
    // N.B. do not ignorePayload() -- new program may well contain IGNORE_MSG constant!

    // save value
    captureH2AOpCode (ProgramDataInc_OC);

    // add to program if room, record if error
    uint32_t v;
    memcpy (&v, &opcode_values[ProgramDataInc_OC], sizeof(v));
    if (!iap_setNextProgram (v)) {
        status_bits |= FLASHError_SBM;
    }

    // echo
    ackOpCode(ProgramDataInc_OC);
}

/* handle receipt of ProgramFLASH opcode
 */
static void runOpCode_ProgramFLASH()
{
    // check if ignore
    if (ignorePayload()) {
        ackOpCode (ProgramFLASH_OC);
        return;
    }

    // save value, although it does not mean anything
    captureH2AOpCode (ProgramFLASH_OC);

    // try to reboot to new program else note and echo
    if (!iap_commit()) {
        status_bits |= FLASHError_SBM;
    }

    // never get here if new program is restarted successfully

    // echo
    ackOpCode(ProgramFLASH_OC);
}



/************************************************************************
 *
 * Master switch case to perform the action required for each DM_OpCode
 *
 *
 ************************************************************************/


/* perform work indicated in h2a_msg opcode.
 * return whether opcode is even valid.
 */
bool run_H2A_OpCode()
{
    DM_OpCode opcode = h2a_msg[OPCODE_OFFSET];
    bool ok = true;

    switch (opcode) {

    // keeping these in numeric order gives the compiler the best opportunity to
    // implement this as a jump table.

    // default processing
    case ProportionalGain_OC:  	// fallthru
    case IntegralGain_OC:  		// fallthru
    case DifferentialGain_OC:  	// fallthru
    case VelDampGain_OC:  		// fallthru
    case OutputGain_OC:  		// fallthru
    case CoilRampPeriod_OC:         // fallthru
    case MinPosition_OC:  		// fallthru
    case MaxPosition_OC:            // fallthru
    case MaxCapRate_OC:
        runOpCode_default (opcode);
        break;

    // save filter settings
    case FilterCapZ0_OC:
        runOpCode_FIR7Filter (&c_filter, &c_filter.z0, opcode);
        break;
    case FilterCapZ1_OC:
        runOpCode_FIR7Filter (&c_filter, &c_filter.z1, opcode);
        break;
    case FilterCapZ2_OC:
        runOpCode_FIR7Filter (&c_filter, &c_filter.z2, opcode);
        break;
    case FilterCapZ3_OC:
        runOpCode_FIR7Filter (&c_filter, &c_filter.z3, opcode);
        break;
    case FilterCapZ4_OC:
        runOpCode_FIR7Filter (&c_filter, &c_filter.z4, opcode);
        break;
    case FilterCapZ5_OC:
        runOpCode_FIR7Filter (&c_filter, &c_filter.z5, opcode);
        break;
    case FilterCapZ6_OC:
        runOpCode_FIR7Filter (&c_filter, &c_filter.z6, opcode);
        break;

    case FilterDiffZ0_OC:
        runOpCode_FIR4Filter (&d_filter, &d_filter.z0, opcode);
        break;
    case FilterDiffZ1_OC:
        runOpCode_FIR4Filter (&d_filter, &d_filter.z1, opcode);
        break;
    case FilterDiffZ2_OC:
        runOpCode_FIR4Filter (&d_filter, &d_filter.z2, opcode);
        break;
    case FilterDiffZ3_OC:
        runOpCode_FIR4Filter (&d_filter, &d_filter.z3, opcode);
        break;

    // save nm conversion coefficients
    case CapCalGain_OC:             // fallthru
    case CapCalOffset_OC:  	        // fallthru
    case CapCalVg_OC:  	        // fallthru
        runOpCode_default (opcode);
        break;

    // just ack, can't set
    case RawCapADC_OC:
        ackOpCode(RawCapADC_OC);
        break;

    // just save and ack
    case WildCoilDt_OC:
        runOpCode_default (opcode);
        break;
    case FFSmoothing_OC:
        runOpCode_default (opcode);
        break;

    // these opcodes each have more unique processing
    case SerialNumber_OC:
        ackOpCode(SerialNumber_OC);
        break;
    case SWVersion_OC:
        ackOpCode(SWVersion_OC);
        break;
    case Temperature_OC:
        runOpCode_Temperature();
        break;
    case StartTuning_OC:
        runOpCode_StartTuning();
        break;
    case CommandedCoil_OC:
        ackOpCode(CommandedCoil_OC);
        break;
    case LEDBlinkRate_OC:
        runOpCode_LEDBlinkRate();
        break;
    case GetOpCode_OC:
        ok = runOpCode_GetOpCode();
        break;
    case Uptime_OC:
        ackOpCode(Uptime_OC);
        break;
    case EStop_OC:
        runOpCode_EStop();
        break;

    // in-app programming
    case ProgramAddr_OC:
        runOpCode_ProgramAddr();
        break;
    case ProgramDataInc_OC:
        runOpCode_ProgramDataInc();
        break;
    case ProgramFLASH_OC:
        runOpCode_ProgramFLASH();
        break;

    // commands
    case RunPosition_OC:
        runOpCode_RunPosition();
        break;
    case FeedForward_OC:
        runOpCode_FeedForward();
        break;
    case RunCoil_OC:
        runOpCode_RunCoil();
        break;

    // just save and ack, no real effect
    case NoOp_OC:
        runOpCode_default (opcode);
        break;

    // anything else is illegal
    default:
        ok = false;
        break;
    }

    // check for updated Ready bits if not already
    if (ok && (status_bits & (RunPosNotReady_SBM | RunCoilNotReady_SBM))) {
        if ((opcodes_seen_mask & REQ_POS_CODES) == REQ_POS_CODES) {
            status_bits &= ~RunPosNotReady_SBM;    // ok to run position now
        }
        if ((opcodes_seen_mask & REQ_COIL_CODES) == REQ_COIL_CODES) {
            status_bits &= ~RunCoilNotReady_SBM;    // ok to run coil now
        }
    }


    // return whether recognized
    return (ok);
}
