/* connect printf() to huart3, the USB debug port
 */

#include "actuator.h"

// #define _USE_STLINK_PROGRAMMER
#ifdef _USE_STLINK_PROGRAMMER

/* use this version with the ST Programmer with texane st-util and gdb
 * As of July 2017 can not get to work.
 */

int _write(int file, char *ptr, int len)
{
    int DataIdx;

    for (DataIdx = 0; DataIdx < len; DataIdx++) {
        ITM_SendChar( *ptr++ );
    }

    return len;
}
#endif // _USE_STLINK_PROGRAMMER


#define _USE_UART3
#ifdef _USE_UART3
/* use this version to print to uart3.
 * if using Nucleo break-off link this will appear as /dev/ttyACM0
 */

/* this function is used by printf to send out the bytes.
 * we use DMA but allow for getting behind.
 */
int _write( int file, char *ptr, int len )
{
    // need stable copy for DMA because printf() will reuse the buffer it passes here
    static volatile uint8_t copy_buffer[100];

    // bale now if nothing to send
    if (len <= 0) {
        return (0);
    }

    // send at most what fits in our buffer
    int n_send = len <= sizeof(copy_buffer) ? len : sizeof(copy_buffer);

    // wait for any previous message to complete
    while (HAL_UART_GetState (&huart3) == HAL_UART_STATE_BUSY_TX) {
        HAL_Delay(1);
    }

    // make stable copy and start DMA
    memcpy ((void*)copy_buffer, ptr, n_send);
    HAL_UART_Transmit_DMA (&huart3, (uint8_t*)copy_buffer, n_send);

    // claim immediate success
    return (n_send);
}

#endif // _USE_UART3
