/* code to initialize and read the capacitor difference from the FPGA.
 */

#include "actuator.h"

/* pin assignments from collection
 */
#define D_STROBE_Pin IO_9_Pin
#define D_STROBE_GPIO_Port IO_9_GPIO_Port
#define D_READY_Pin IO_10_Pin
#define D_READY_GPIO_Port IO_10_GPIO_Port
#define SPI_SS_B_Pin IO_11_Pin
#define SPI_SS_B_GPIO_Port IO_11_GPIO_Port


static void Send_Byte (uint8_t byte)
{
    for (uint8_t i = 0; i < 8; i++) {
        // send MSB
        Set_Port (SPI_MOSI_GPIO_Port, SPI_MOSI_Pin, 0 != (byte & (1 << 7)));

        // shift left
        byte <<= 1;

        // clock
        Set_Port (SPI_SCK_GPIO_Port, SPI_SCK_Pin, false);
        nSec_Delay (100);
        Set_Port (SPI_SCK_GPIO_Port, SPI_SCK_Pin, true);
        nSec_Delay (100);
    }
}

static void Send_Clocks (int n)
{
    for (int i = 0; i < n; i++) {
        Set_Port (SPI_SCK_GPIO_Port, SPI_SCK_Pin, false);
        nSec_Delay (100);
        Set_Port (SPI_SCK_GPIO_Port, SPI_SCK_Pin, true);
        nSec_Delay (100);
    }
}

static void Send_File (void)
{
    for (int i = 0; i < fpga_program_size; i++) {
        Send_Byte (fpga_program[i]);
    }
}


/* send fpga_program[fpga_program_size] to FPGA.
 * set FPGALoadError_SBM if trouble.
 * based on sample code in Lattice App note iCE40ProgrammingandConfiguration.pdf, page 25
 */
void bootFPGA()
{
    // configure device for programming

    Set_Port (SPI_SCK_GPIO_Port, SPI_SCK_Pin, true);
    Set_Port (CRESET_GPIO_Port, CRESET_Pin, false);
    Set_Port (SPI_SS_B_GPIO_Port, SPI_SS_B_Pin, false);
    nSec_Delay(500);
    Set_Port (CRESET_GPIO_Port, CRESET_Pin, true);
    uSec_Delay(1000);
    Send_Clocks (8);

    // send program
    Send_File();
    Send_Clocks (100);

    // change SPI_SS_B to input for debug
    setPinAsInput (SPI_SS_B_GPIO_Port, SPI_SS_B_Pin);


    // check success
    static const uint32_t FPGA_TO_CT = 10000000UL*MHz;  // 10 seconds
    uint32_t t0 = getClock();
    while (getClock() - t0 < FPGA_TO_CT) {
        if (Get_Port (CDONE_GPIO_Port, CDONE_Pin)) {
            // meep meep
            FAST_WR_PIN (LED_GPIO_Port, LED_Pin, GPIO_PIN_SET);
            HAL_Delay(150);
            FAST_WR_PIN (LED_GPIO_Port, LED_Pin, GPIO_PIN_RESET);
            HAL_Delay(50);
            FAST_WR_PIN (LED_GPIO_Port, LED_Pin, GPIO_PIN_SET);
            HAL_Delay(150);
            FAST_WR_PIN (LED_GPIO_Port, LED_Pin, GPIO_PIN_RESET);
            HAL_Delay(1000);

            // ok
            return;
        }
        HAL_Delay(100);
    }

    // fail
    status_bits |= FPGALoadError_SBM;
}


/* called to read ADC difference from FPGA.
 * the value increases as the two capacitor plates get closer.
 * return whether read was ok and note if not.
 */
bool readFPGA(uint16_t *value)
{
    // check for ready else flag error
    bool fpga_ready = (FAST_RD_PIN (D_READY_GPIO_Port, D_READY_Pin) == GPIO_PIN_SET);
    if (!fpga_ready || (status_bits & FPGALoadError_SBM)) {
        status_bits |= CapReadErr_SBM;
        return (false);
    }

    // read LSB from bus first.
    // N.B. IDR is a 32 bit register, port F uses lower 16 bits, FPGA uses lower 8 of those
    *value = GPIOF->IDR & 0xFF;

    // pulse D_STROBE to inform FPGA it can put MSB on bus.
    HAL_GPIO_WritePin (D_STROBE_GPIO_Port, D_STROBE_Pin, GPIO_PIN_SET);
    nSec_Delay(40);
    HAL_GPIO_WritePin (D_STROBE_GPIO_Port, D_STROBE_Pin, GPIO_PIN_RESET);
    nSec_Delay(40);

    // read and fold in MSB
    *value |= (GPIOF->IDR & 0xFF) << 8U;

    // ok
    return (true);
}
