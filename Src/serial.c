/* set SerialNumber_OC by reading the 1-wire chip.
 * set SerialNumberErr_SB if trouble
 */

#include "actuator.h"

#define N_SN_TRIES      10             // number of attempts to read serial number

/* recommended 1-wire software times, usecs.
 * See "1-Wire Communication Through Software" -- Maxim App note 126
 * https://www.maximintegrated.com/en/app-notes/index.mvp/id/126
 */
#define DELAY_A 6
#define DELAY_B 64
#define DELAY_C 60
#define DELAY_D 10
#define DELAY_E 9
#define DELAY_F 55
#define DELAY_G 0
#define DELAY_H 480
#define DELAY_I 70
#define DELAY_J 410

/* find MAXIM CRC-8 of the nbyte DS24*1 serial number + family.
 * from https://github.com/madler/crcany
 */
static uint8_t crc8maxim (uint8_t *maxsn, uint8_t nbytes)
{
    uint8_t crc = 0;
    for (uint8_t i = 0; i < nbytes; i++) {
        crc ^= *maxsn++;
        for (uint8_t k = 0; k < 8; k++) {
            crc = crc & 1 ? (crc >> 1) ^ 0x8c : crc >> 1;
        }
    }
    return (crc);
}

/* set SerialNumber_OC else set SerialNumberErr_SBM
 */
void getSerialNumber()
{
    // insure time to fully powered on
    uSec_Delay (2000);

    // N.B. we always leave the port as open collect output. Then before reading, we set output high
    // so the SN chip has control.
    setPinAsOutput (DALLAS_SN_GPIO_Port, DALLAS_SN_Pin);

    for (uint8_t try = 0; try < N_SN_TRIES; try++) {

                    // reset
                    uSec_Delay (DELAY_G);
                    HAL_GPIO_WritePin (DALLAS_SN_GPIO_Port, DALLAS_SN_Pin, GPIO_PIN_RESET);
                    uSec_Delay (DELAY_H);
                    HAL_GPIO_WritePin (DALLAS_SN_GPIO_Port, DALLAS_SN_Pin, GPIO_PIN_SET);
                    uSec_Delay (DELAY_I);
                    if (HAL_GPIO_ReadPin (DALLAS_SN_GPIO_Port, DALLAS_SN_Pin) != GPIO_PIN_RESET) {
                        continue;
                    }
                    uSec_Delay (DELAY_J);

                    // send Read ROM command 0x33
                    for (uint8_t i = 0; i < 8; i++) {
                        if (0x33 & (1<<i)) {
                            // write 1
                            HAL_GPIO_WritePin (DALLAS_SN_GPIO_Port, DALLAS_SN_Pin, GPIO_PIN_RESET);
                            uSec_Delay (DELAY_A);
                            HAL_GPIO_WritePin (DALLAS_SN_GPIO_Port, DALLAS_SN_Pin, GPIO_PIN_SET);
                            uSec_Delay (DELAY_B);
                        } else {
                            // write 0
                            HAL_GPIO_WritePin (DALLAS_SN_GPIO_Port, DALLAS_SN_Pin, GPIO_PIN_RESET);
                            uSec_Delay (DELAY_C);
                            HAL_GPIO_WritePin (DALLAS_SN_GPIO_Port, DALLAS_SN_Pin, GPIO_PIN_SET);
                            uSec_Delay (DELAY_D);
                        }
                    }

                    // clock out 64 bits: first 56 are family and serial number, last 8 are CRC
                    uint8_t bits[8];
                    memset (bits, 0, sizeof(bits));
                    for (uint8_t i = 0; i < 64; i++) {
                        HAL_GPIO_WritePin (DALLAS_SN_GPIO_Port, DALLAS_SN_Pin, GPIO_PIN_RESET);
                        uSec_Delay (DELAY_A);

                        HAL_GPIO_WritePin (DALLAS_SN_GPIO_Port, DALLAS_SN_Pin, GPIO_PIN_SET);
                        uSec_Delay (DELAY_E);
                        uint8_t bit = HAL_GPIO_ReadPin (DALLAS_SN_GPIO_Port, DALLAS_SN_Pin) == GPIO_PIN_SET;
                        bits[i/8] |= (bit << (i%8));
                        uSec_Delay (DELAY_F);
                    }

                    // CRC of message including CRC byte should give 0
                    if (crc8maxim (bits,8) == 0) {
                        // success! just use least 16 bits above family code as serial number, hopefully these change
                        // enough across all parts.
                        uint32_t sn = ((uint32_t)bits[2] << 8) | bits[1];
                        memcpy (&opcode_values[SerialNumber_OC], &sn, sizeof(uint32_t));
                        return;
                    }
                }

    // if get here we failed
    opcode_values[SerialNumber_OC] = 0;
    status_bits |= SerialNumberErr_SBM;
}
