/* low-level coil control functions.
 */

#include "actuator.h"

/* If the coil is wired backwards, change both the H bridge sign and the read coil sign.
 * This will keep the postion loop feedback and the run coil commands unchanged.
 */

/* maximum PWM period: half Go period - margin for ADC conversion
 */
#define TIM2_BUS_MHZ    (MHz/2U)                    // timer counts per usec (runs @ half CPU)
static const uint32_t MAX_PWM = 4*TIM2_BUS_MHZ;     // half Go cyclei: 5 us - margin

/* supporting data for monitoring for wild MeasuredCoil changes.
 * the idea is to capture rising events above WILD_PULL and decreasing events below WILD_PUSH
 * then watch for N_WILD pairs within WILD_DT each
 */
#define N_WILD                  5                               // number of PULL<->PUSH cycles for trigger
#define WILD_DT   (opcode_values[WildCoilDt_OC]*CT_PER_SEC/2)   // max clock ticks per half cycle
#define WILD_PULL               (0.5F)                          // high pull current threshold
#define WILD_PUSH               (-0.5F)                         // high push current threshold
static uint32_t prev_t;                                         // clock ticks at prev event
static float prev_mc;                                           // previous MC, to find crossing events
static bool prev_was_pull;                                      // whether prev event was pull, else push
static int n_events;                                            // n alternating push/pull events

/* check for wild swings of MeasuredCoil_OC.
 * if so, go to safe current and set WildCurrent_SBM
 */
static void checkWildCoil()
{
    // handy
    float mc = opcode_values[MeasuredCoil_OC];
    uint32_t now = getClock();

    // check events
    if (mc > WILD_PULL && prev_mc <= WILD_PULL) {

        // just saw pull event, record if first or prev was push and recent enough
        if (n_events == 0 || (!prev_was_pull && now - prev_t < WILD_DT)) {
            prev_was_pull = true;
            prev_t = now;
            n_events++;
        } else {
            n_events = 0;
        }

    } else if (mc < WILD_PUSH && prev_mc >= WILD_PUSH) {

        // just saw push event, record if first or prev was pull and recent enough
        if (n_events == 0 || (prev_was_pull && now - prev_t < WILD_DT)) {
            prev_was_pull = false;
            prev_t = now;
            n_events++;
        } else {
            n_events = 0;
        }
    }

    // take evasive action if sufficient events
    if (n_events >= N_WILD) {

        // immediately ramp to safe
        forceSafeCoil();

        // start holding period and record
        wildhold_t0 = getClock();
        status_bits |= WildCoil_SBM;

        // reset
        n_events = 0;
    }

    // persist
    prev_mc = mc;
}

/* set the coil current from opcode_values[CommandedCoil] which is an interval [-1,1].
 * magnitude determines PWM duty cycle, sign determines H bridge phase.
 * N.B. we are called twice per inner loop via Go interrupt so only engage half the desired period each time
 */
void commandCoil()
{
    // find sign and magnitude
    float magnitude = opcode_values[CommandedCoil_OC];
    bool is_neg = false;
    if (magnitude < 0) {
        is_neg = true;
        magnitude = -magnitude;
    }

    // set PWM duty cycle from magnitude
    uint32_t on_period = (uint32_t)(magnitude*MAX_PWM);             // fraction of max period
    __HAL_TIM_SET_COMPARE (&htim2, TIM_CHANNEL_4, on_period);       // period starting next cycle
    __HAL_TIM_SET_COUNTER (&htim2, 0UL);                            // start next cycle


    // set H bridge direction from sign
    GPIO_PinState dir = is_neg ? GPIO_PIN_SET : GPIO_PIN_RESET;     // flip a coin
    FAST_WR_PIN(PWM_DIR_GPIO_Port, PWM_DIR_Pin, dir);

}

/* force immediate Coil state at safe current
 */
void forceSafeCoil()
{
    status_bits &= ~(PositionElseCoil_SBM | RunCoilNotReady_SBM);
    opcode_values[RunCoil_OC] = FC_VALUE;
    opcode_values[CommandedCoil_OC] = FC_VALUE;
}


// use DMA to continuously read coil ADC, then pick up any time with no delay
static uint16_t coil_dma_buffer;

/* initialize coil reading system
 */
void initCoilCurrent()
{
    HAL_ADC_Start_DMA (&hadc2, (uint32_t*)&coil_dma_buffer, 1); // n half-words
}

/* measure and save the normalized measured coil current in opcode_values[MeasuredCoil].
 */
void readCoilCurrent()
{
    // read and scale [0,4095] to [-1,1]

    // inject fake value if testing
    if (opcode_values[StartTuning_OC] == 2) {
        static uint16_t fake_mc_value;
        opcode_values[MeasuredCoil_OC] = (2.0F/4095.0F)*(++fake_mc_value & 0xFFF) - 1.0F;
    } else {
        opcode_values[MeasuredCoil_OC] = 1.0F - (2.0F/4095.0F)*coil_dma_buffer; // flip a coin
    }

    // check for safety
    checkWildCoil();
}
