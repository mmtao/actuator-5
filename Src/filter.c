/* function to execute FIR filters.
 */

#include "actuator.h"


/* initialize the history entries in the given FIR 4 filter
 */
void initFIR4Filter (FIR4Filter *fp, float v0)
{
    fp->history[2] = fp->history[1] = fp->history[0] = v0;
}

/* process one value through the given FIR4 filter.
 * measured at 0.28 usec
 */
float runFIR4Filter (FIR4Filter *fp, float v)
{
    // run the filter
    float sum = fp->z0 * v;
    sum += fp->z1 * fp->history[0];
    sum += fp->z2 * fp->history[1];
    sum += fp->z3 * fp->history[2];

    // roll history
    fp->history[2] = fp->history[1];
    fp->history[1] = fp->history[0];
    fp->history[0] = v;

    return (sum);
}


/* initialize the history entries in the given FIR 7 filter
 */
void initFIR7Filter (FIR7Filter *fp, float v0)
{
    fp->history[5] = fp->history[4] = fp->history[3] =
                                          fp->history[2] = fp->history[1] = fp->history[0] = v0;
}

/* process one value through the given FIR7 filter.
 */
float runFIR7Filter (FIR7Filter *fp, float v)
{
    // run the filter
    float sum = fp->z0 * v;
    sum += fp->z1 * fp->history[0];
    sum += fp->z2 * fp->history[1];
    sum += fp->z3 * fp->history[2];
    sum += fp->z4 * fp->history[3];
    sum += fp->z5 * fp->history[4];
    sum += fp->z6 * fp->history[5];

    // roll history
    fp->history[5] = fp->history[4];
    fp->history[4] = fp->history[3];
    fp->history[3] = fp->history[2];
    fp->history[2] = fp->history[1];
    fp->history[1] = fp->history[0];
    fp->history[0] = v;

    return (sum);
}
