/* main code for Actuator control.
 *
 * We are called the "Actuator". We communicate with the central "hub" or "CH". We often use the terms
 * A2H to mean Actuator-to-Hub and H2A to mean Hub-to-Actuator messages.
 *
 * initActuator() is called once at powerup for any one-time setup.
 * runActuator() is then called repeatedly forever.
 *
 * Basic idea is to spin at 100 kHz performing one update each cycle for the current state. States are
 * Position and Coil, initially the latter. Messages may arrive from CH any time in h2a_msg[]. First byte is
 * an opcode which gets processed and may change our state. All messages are intentionally the same size
 * so we can use DMA. Outbound messages are built in a2h_msg, see below.
 *
 * Inner loop is synchronized to external "Go" signal on PB0, expected to be running at about 100 kHz. Each
 * actual measured period is in loop_period_us for use by the control law differentiator and integrator.
 *
 * a2h_msg and h2a_msg are both 6 byte arrays arranged as follows:
 * N.B. never assume the payload can be used directly from within the array, always memcpy to/from
 *      a declared value of the expected type to void alignment issues
 *   0  : one of DM_OpCode value
 *   1-4: single-precision float payload, or 4-byte unsigned integer; always little-endian
 *   5  : sum of previous 5 bytes
 *
 * The "usual" pattern of a2h messages is MeasuredPosition, MeauredCoil and StatusBits in circular order, sent
 * every 30 Go ticks (ie, 300 usecs). However, receipt of any valid h2a message makes two temporary
 * changes to this pattern:
 *   1. the cycle is forced to restart at MeasuredPosition which is sent 15 Go ticks after receipt,
 *      then the normal 30 cycle message period resumes;
 *   2. the next StatusBits message is replaced with the opcode of the received message to serve as an
 *      ack; then subsequent StatusBits messages resume as usual.
 *   3. there is actually a 3rd variation: if the h2a opcode is GetOpCode, then the opcode for the
 *      replaced StatusBits message is the opcode within the GetOpCode payload. This allows any opcode to
 *      be queried.
 *
 * The "unusual" pattern for a2h messages occurs when a StartTuning message arrives with a payload > 0.
 * While tuning is in progress, a2h is sent every third Go rising edge with opcode TuningData; no
 * other messages are sent until/unless StartTuning is received with payload 0. The payload for TuningData
 * is a packed combination of MeasuredPos and MeasuredCoil. MeasuredPos is packed in the lower 16 bits as
 * an unsigned value [0,65535] mapped linearly from [MinPosition,MaxPosition]. MeasuredCoil is packed into
 * the upper 16 bits as an unsigned value [0,65534] mapped from [-1,1]. The special value 65535 indicates
 * that the lower 16 bits is actually RunPosition, not MeasuredPosition. This special value is used
 * one time immedately after receipt of a RunPosition and then MeasuredPos values resume. The reason for
 * this is so when the TuningData messages are packaged by the CH for transmission to the RC, the RC
 * can tell precisionly when the RunPosition was commanded in the collection of TuningData values.
 *
 * When first powered on we send no messages until having received one. This is to avoid sending messages
 * to CH before both 30-Go counters are in sync.
 *
 * A coil is controlled by changing the duty cycle of a PWM signal, the polarity of which can be changed
 * with an H-bridge. The Go clock also charges and discharges a capacitor. An FPGA captures each value with
 * an external ADC. The difference in the two values is sent to us for use as the measured capacitor value.
 * The ADC capture occurs just before each Go edge. To avoid the ADC capture occuring during a
 * PWM transition, each PWM period begins at each Go edge and lasts a maximum of a little less than one Go
 * half-cycle.
 *
 * Each Actuator has a unique serial number, implemented by reading a Dallas Semiconductor 1-wire serial
 * number device.
 *
 * Summary of IO assignments:
 *   Messages from the central hub (h2a) use UART5 RX with DMA on PD2 (UART TX unused).
 *   Messages to the central hub (a2h) use UART4 TX with DMA on PC10 (UART RX unused).
 *   In case an ST-Link is attached UART3 TX is available via printf, PB10.
 *   Coil current is programmed using PWM with TIM2 CH4 output on PB2 and H-Bridge direction on PB1.
 *   Go clock arrives on input PB0.
 *   Coil current sensor on ADC2 IN5 PA5 is read continuously with DMA.
 *   Diagnostics LED on PB7.
 *   FPGA uses many lines, see schematic.
 * Other peripherals (no external pins):
 *   Internal temperature sensor on ADC1 polled as needed.
 *
 */


// glue for Actuator project files
#include "actuator.h"



/*******************************************************************************
 *
 * constants
 *
 *******************************************************************************/

#define MT_FIRST_GT 15             	// Go ticks after any H2A msg receipt to send first A2H message
#define MT_OTHER_GT 30             	// Go ticks thereafter to send subsequent A2H messages
#define HB_TO_S     60                  // HBTimeout timeout, seconds TODO
#define MAX_LOOP_TIME_CT (11*CT_PER_GO/10) // max allowed loop time allowing for jitter, clock ticks


/*******************************************************************************
 *
 * system timing and sync support
 *
 *******************************************************************************/

float loop_period_us;			// last measured loop period, microseconds
uint32_t wildhold_t0;                   // clock ticks when WildCoil hold period started
uint32_t led_counter, led_count;        // led flashing control
static volatile uint32_t loop_start_ct; // clock tick at start of current inner loop
static volatile uint32_t prev_start_ct;	// previous loop_start_ct
static volatile bool go_rise;           // set in interrupt when see GO clock rising edge
static uint32_t msg_ticks;		// Go tick normal message down-counter; init to 0 to wait for 1st h2a
static uint32_t up_ct;                  // uptime, clock ticks
static uint8_t hb_downctr;              // Heartbeat down counter, seconds


/*******************************************************************************
 *
 * persistent data
 *
 *******************************************************************************/

// state and opcode management
static DM_OpCode cycle_a2h_opcode;	// next normal a2h_msg opcode to send
DM_OpCode next_a2h_status;		// next StatusBits substitute opcode
float opcode_values[N_ACTUATOR_OC];	// storage for opcodes that have an associated value
uint32_t status_bits;			// integer shadow of opcode_values[StatusBits]
static float captured_coil;             // MeasuredCoil at time of sending MeasuredPosition
bool new_run_pos;                       // set whenever we see RunPosition

// message buffers management
static volatile bool h2a_ready;		// flag new incoming message in h2a_msg is ready
static volatile bool h2a_error;		// flag framing error detected in incoming h2a_msg
uint8_t h2a_msg[H2A_MSG_LEN];		// Hub to Actuator message
uint8_t a2h_msg[A2H_MSG_LEN];		// normal Actuator to Hub message
static uint8_t a2h_db_msg[A2H_MSG_LEN];	// 2nd double-buffered a2h used for tuning
uint8_t *a2h_db_toggle;                 // point to a2h_msg or a2h_db_msg if tuning

// FIR filters
FIR7Filter c_filter;			// capacitor
FIR4Filter d_filter;			// PID derivative gain




/*******************************************************************************
 *
 * support functions to implement the main inner loop
 *
 *******************************************************************************/



/* flash LED (PB7) if enabled
 */
static void flashLED()
{
    if (opcode_values[LEDBlinkRate_OC] > 0) {

        // led_counter is incremented in the Go interrupt handler

        if (led_counter < led_count/10) {
            // on 10% of the time
            if (!(status_bits & LEDIsOn_SBM)) {
                FAST_WR_PIN(LED_GPIO_Port, LED_Pin, GPIO_PIN_SET);
                status_bits |= LEDIsOn_SBM;
            }
        } else if (led_counter < led_count) {
            // off 90% of the time
            if (status_bits & LEDIsOn_SBM) {
                FAST_WR_PIN(LED_GPIO_Port, LED_Pin, GPIO_PIN_RESET);
                status_bits &= ~LEDIsOn_SBM;
            }
        } else {
            // start again
            led_counter = 0;
        }

    } else {

        // insure off
        if (status_bits & LEDIsOn_SBM) {
            FAST_WR_PIN(LED_GPIO_Port, LED_Pin, GPIO_PIN_RESET);
            status_bits &= ~LEDIsOn_SBM;
        }

    }
}

/* interrupt handler called on rising or falling edge of Go clock
 */
void GOInterrupt()
{
    // get time at interrupt
    uint32_t t0 = getClock();

    // update next half-pwm cycle ASAP after both edges
    commandCoil();

    // update timing only on rising edge
    if (FAST_RD_PIN (GO_IN_GPIO_Port, GO_IN_Pin) == GPIO_PIN_SET) {
        // calibrate time at rising moment
        prev_start_ct = loop_start_ct;      // roll previous start time
        loop_start_ct = t0;                 // set Go edge time
        go_rise = true;                     // note rising edge has occurred
        led_counter++;                      // update LED timer
    }
}

/* spin until see next low-to-high GO_IN pin transition.
 * then update period and uptime.
 */
static void syncToGoRise()
{
    // wait for Go clock flag from interrupt
    uint32_t t0 = getClock();
    while (!go_rise) {
        if (getClock() - t0 > CT_PER_GO) {
            // never happens means there is probably no clock signal
            status_bits |= NoGo_SBM;
            break;
        }
    }
    go_rise = false;

    // update elapsed period and advance uptime, note if too long
    uint32_t loop_dt_ct = loop_start_ct - prev_start_ct;
    if (loop_dt_ct > MAX_LOOP_TIME_CT) {
        status_bits |= TimingError_SBM;
    }
    loop_period_us = CT2FUS(loop_dt_ct);
    if ((up_ct += loop_dt_ct) >= CT_PER_SEC) {
        // update up time and heartbeat
        opcode_values[Uptime_OC] += 1;
        up_ct -= CT_PER_SEC;
        if (hb_downctr > 0) {
            hb_downctr -= 1;
        }
    }
}

/* perform another iteration of the current run state.
 * N.B. state _transitions_ are handled by runopcode.c
 */
static void updateState()
{
    if (status_bits & PositionElseCoil_SBM) {
        // run next iteration of Position control law
        updatePositionState();
    } else {
        // run next iteration of coil control law
        updateCoilState();
    }
}


/* interrupt called when UART5 encounters a receive error such as framing.
 */
void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{
    // only respond to uart5
    if (huart != &huart5) {
        return;
    }

    // clear interrupt source
    __HAL_UART_CLEAR_PEFLAG (huart);

    // set flag
    h2a_error = true;
}


/* interrupt called when UART5's receive DMA engine says a new h2a_msg is available.
 * just set h2a_ready flag for checkH2AMessage()
 */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
    // only respond to uart5
    if (huart != &huart5) {
        return;
    }

    // set flag
    h2a_ready = true;
}


/* check for a new h2a_msg message to arrive based on h2a_ready.
 * note corrupt messages and timeouts, process new command if good.
 * always return with DMA prepared for another incoming message.
 * return whether a new DMA had to be set up -- it takes a long time.
 */
static bool checkH2AMessage()
{
    // read new message if ready
    bool start_dma = false;

    if (h2a_error) {

        // note error and reset h2a message error flag
        status_bits |= BadChecksum_SBM;
        h2a_error = false;
        // printf ("frame: %2d | 0x%02x%02x%02x%02x | %2d\n", h2a_msg[0], h2a_msg[4], h2a_msg[3], h2a_msg[2], h2a_msg[1], h2a_msg[5]);

        // clear all errors then rearm DMA
        HAL_UART_Abort (&huart5);
        HAL_UART_Receive_DMA (&huart5, h2a_msg, H2A_MSG_LEN);
        start_dma = true;
    }

    if (h2a_ready) {

        // printf ("ready: %2d | 0x%02x%02x%02x%02x | %2d\n", h2a_msg[0], h2a_msg[4], h2a_msg[3], h2a_msg[2], h2a_msg[1], h2a_msg[5]);

        // received something
        if (checksumOk (h2a_msg)) {
            // checksum is ok, process opcode, note if bad
            if (run_H2A_OpCode()) {
                // valid message
                msg_ticks = MT_FIRST_GT;                    	// resync timing for next a2h_msg
                cycle_a2h_opcode = MeasuredPosition_OC;         // reset a2h message cycle
                hb_downctr = HB_TO_S;                           // refresh heartbeat down counter
                status_bits &= ~HBTimeout_SBM;                  // clear heartbeat flag
            } else {
                // note receipt of an invalid command code
                status_bits |= BadOpCode_SBM;
            }
        } else {
            // note bad checksum
            status_bits |= BadChecksum_SBM;
        }

        // reset h2a message ready flag
        h2a_ready = false;

        // arm fresh DMA to receive next message
        HAL_UART_Receive_DMA (&huart5, h2a_msg, H2A_MSG_LEN);
        start_dma = true;

    }

    return (start_dma);
}

/* check for heartbeat. if time out, go back to coil mode with default current
 */
static void checkHeartbeat()
{
    if (hb_downctr == 0) {

        // reset down counter
        hb_downctr = HB_TO_S;

        // flag heart beat timeout!
        status_bits |= HBTimeout_SBM;

        // force valid Coil state applying standby current
        forceSafeCoil();
    }
}

/* send TuningData message.
 *
 * transmission takes longer than one Go cycle so we only send every third Go clock:
 *   cycle 0: quiet time for accurate cap read
 *   cycle 1: build message and transmit
 *   cycle 2: remain idle during remainder of transmission
 *
 * use double-buffering with a2h_db_msg so we can compute next message while first one is still transmitting.
 * N.B. a2h_db_toggle is set to NULL when we get StartTuning so we know not to send a stale message.
 *
 * The payload is normally MeasuredCoil in the high word and MeasuredPosition in the low word. However,
 * if new_run_pos is set (indicating a new RunPosition has been seen) then RunPosition replaces MeasuredPos
 * and the high "coil" word is set to a special flag value of 65536.
 *
 * return whether we actually sent a message.
 */
static bool sendTuningMessage ()
{
    // rolling cycle number
    static uint8_t cycle;

    // sent
    bool sent = false;

    // increment with wrap, or init if first
    if (!a2h_db_toggle) {
        cycle = 0;
        a2h_db_toggle = a2h_msg;
    } else {
        if (++cycle == 3) {
            cycle = 0;
        }
    }

    if (cycle == 1) {

        // build message

        // get MeasuredCoil [-1,1] mapped to [0,65534], 65535 is reserved to flag low word is really RunPos
#define COIL_SCALE (65534.0F/2.0F)              // 65535 is reserved for new_run_pos
        uint16_t coil_word = COIL_SCALE * (opcode_values[MeasuredCoil_OC] + 1.0F);

        // get MeasuredPos unless new_run_pos then use RunPos
        float pos;
        if (new_run_pos) {
            coil_word = 65535U;                         // flag indicating cap_word is RunPosition
            pos = opcode_values[RunPosition_OC];        // report RunPosition
            new_run_pos = false;                        // reset until another RunPosition
        } else {
            pos = opcode_values[MeasuredPosition_OC];   // report normal MeasuredPos
        }

        // remap position pos [MinPosition,MaxPosition] to [0,65535]
        // N.B. we assume this range is already guaranteed
        uint16_t cap_word = 65535.0F * (pos - opcode_values[MinPosition_OC])
                            / (opcode_values[MaxPosition_OC] - opcode_values[MinPosition_OC]);

        // combine so measured coil is hi word, cap position is low word
        uint32_t combined = (((uint32_t)coil_word) << 16U) | cap_word;
        a2h_db_toggle[OPCODE_OFFSET] = TuningData_OC;
        memcpy (&a2h_db_toggle[PAYLOAD_OFFSET+0], &combined, sizeof(uint32_t));

        // add checksum
        setChecksum (a2h_db_toggle);

        // transmit

        HAL_UART_Transmit_DMA (&huart4, a2h_db_toggle, A2H_MSG_LEN);
        sent = true;

        // swap buffers
        if (a2h_db_toggle == a2h_msg) {
            a2h_db_toggle = a2h_db_msg;
        } else {
            a2h_db_toggle = a2h_msg;
        }

    }

    return (sent);
}


/* transmit normal message to hub if it is time to do so.
 *
 * return whether we actually sent a message.
 */
static bool sendNormalMessage()
{
    // only send if msg_ticks _transitions_ down to 0
    if (msg_ticks == 0 || --msg_ticks > 0) {
        return(false);
    }

    // reset ticker for next routine message interval
    msg_ticks = MT_OTHER_GT;

    // wait for any previous DMA to complete
    while (HAL_UART_GetState (&huart4) == HAL_UART_STATE_BUSY_TX) {
        continue;
    }

    // select opcode to send, could be substituded if due for StatusBits
    DM_OpCode send_opcode = cycle_a2h_opcode;
    if (cycle_a2h_opcode == StatusBits_OC) {
        // use the substitute then reset
        send_opcode = next_a2h_status;
        next_a2h_status = StatusBits_OC;
    }

    // set opcode
    a2h_msg[OPCODE_OFFSET] = (uint8_t) send_opcode;

    // set payload
    switch (send_opcode) {
    case StatusBits_OC:
        // payload is local int copy of StatusBits
        memcpy (&a2h_msg[PAYLOAD_OFFSET], (void*)&status_bits, PAYLOAD_LEN);
        // reset the per-message cumulative bits
        status_bits &= ~DM_RESET_STATUS_BM;
        break;
    case MeasuredPosition_OC:
        // payload is from opcode value as usual
        memcpy (&a2h_msg[PAYLOAD_OFFSET], (void*)&opcode_values[MeasuredPosition_OC], PAYLOAD_LEN);
        // and also capture MeasuredCoil at this same moment
        captured_coil = opcode_values[MeasuredCoil_OC];
        break;
    case MeasuredCoil_OC:
        // use value captured at time of MeasuredPosition
        memcpy (&a2h_msg[PAYLOAD_OFFSET], (void*)&captured_coil, PAYLOAD_LEN);
        break;
    default:
        // other opcodes are just copied from their values entry
        memcpy (&a2h_msg[PAYLOAD_OFFSET], (void*)&opcode_values[send_opcode], PAYLOAD_LEN);
    }


    // set checksum
    setChecksum (a2h_msg);

    // start asynchronous DMA
    HAL_UART_Transmit_DMA (&huart4, a2h_msg, A2H_MSG_LEN);

    // roll to next opcode
    switch (cycle_a2h_opcode) {
    case MeasuredPosition_OC:
        cycle_a2h_opcode = MeasuredCoil_OC;
        break;
    case MeasuredCoil_OC:
        cycle_a2h_opcode = StatusBits_OC;
        break;
    case StatusBits_OC:
    default:    // lint
        cycle_a2h_opcode = MeasuredPosition_OC;
        break;
    }

    // sent
    return (true);
}


/* transmit message to hub if it is time to do so.
 *
 * return whether we actually sent a message.
 */
static bool sendA2HMessage()
{
    if (status_bits & TuningActive_SBM) {
        return (sendTuningMessage());
    } else {
        return (sendNormalMessage());
    }
}



/*******************************************************************************
 *
 * the two entry points called by main()
 *
 *******************************************************************************/

/* called exactly one time to perform initialization
 */
void initActuator()
{
    // set up for getClock() and friends
    CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
    DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;
    DWT->CYCCNT = 0;
    if (HAL_RCC_GetHCLKFreq() != 1000000UL*MHz) {
        printf ("Clock is not 180 MHz = %lu\n", HAL_RCC_GetHCLKFreq());
        while(1);
    }
    printf ("Clock is %lu Hz\n", HAL_RCC_GetHCLKFreq());

    // init heartbeat down counter
    hb_downctr = HB_TO_S;

    // set sw version
    opcode_values[SWVersion_OC] = SW_VERSION;

    // init status bits
    status_bits = (RunPosNotReady_SBM | RunCoilNotReady_SBM);

    // set SerialNumber_OC
    getSerialNumber();

    // spin up the fpga
    bootFPGA();

    // init a2h message sequence
    cycle_a2h_opcode = MeasuredPosition_OC;
    next_a2h_status = StatusBits_OC;

    // start the PWM
    HAL_TIM_PWM_Start (&htim2, TIM_CHANNEL_4);

    // start ADC1 (temperature sensor)
    initTemperature();

    // start ADC2 (current sensor)
    initCoilCurrent();

    // start h2a receiver
    HAL_UART_Receive_DMA (&huart5, h2a_msg, H2A_MSG_LEN);

    // start in Coil state ramping from 0
    status_bits &= ~PositionElseCoil_SBM;
    opcode_values[CommandedCoil_OC] = 0;
    opcode_values[RunCoil_OC] = FC_VALUE;
    opcode_values[CoilRampPeriod_OC] = FC_PERIOD;

    // start flashing for build confirm, insure starts off
    status_bits &= ~LEDIsOn_SBM;
    HAL_GPIO_WritePin (LED_GPIO_Port, LED_Pin, GPIO_PIN_RESET);
    opcode_values[LEDBlinkRate_OC] = 1.0;                       // Hz
    led_count = GO_PER_SEC / opcode_values[LEDBlinkRate_OC];    // same as runOpCode_LEDBlinkRate()
}

/* called repeatedly forever to perform next loop.
 * considerations:
 *   [ ] DMA setup takes so long not enough time in 1 cycle to do both inbound and outbound.
 *   [ ] doing either adds noise to cap so discard for two cycles.
 */
void runActuator()
{
    // discard 2 cap reads after receiving or sending a message
    static uint8_t n_io;

    /***************************************************
     * start next inner loop synced to rising Go signal
     ***************************************************/
    syncToGoRise();

    // process and note new inbound hub message
    bool rx = checkH2AMessage();

    // send and note any pending outbound message; not enough time to tx and rx in one cycle
    bool tx = false;
    if (!rx) {
        tx = sendA2HMessage();
    }

    // count down after either
    if (rx || tx) {
        n_io = 2;
    } else if (n_io > 0) {
        n_io--;
    }

    // always read cap FPGA to drain message but retain previous if soon after io
    uint16_t capv;
    if (readFPGA (&capv) && n_io == 0) {
        opcode_values[RawCapADC_OC] = capv;
        cleanupCapADC(capv);
    }

    // get MeasuredCoil
    readCoilCurrent();

    // run another iteration of our current state
    updateState();

    // check heartbeat
    checkHeartbeat();

    // display flashing LED
    flashLED();
}
