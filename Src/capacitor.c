/* functions to read the capacitor difference from the FPGA and convert to nm position
 */

#include <limits.h>

#include "actuator.h"

/* convert ADC to nm using the CapCal values
 */
static float ADC2nm (float adc)
{
    float num = opcode_values[CapCalGain_OC] * opcode_values[CapCalVg_OC];
    float den = adc + opcode_values[CapCalOffset_OC];

    // include < 0 so value doesn't go negative and the later limit clamps to min value
    float nm = (den <= 0 ? opcode_values[MaxPosition_OC] : num/den);

    return (nm);
}

/* given a net ADC set MeasuredPosition in nm.
 * cleaning steps are:
 *   convert to nm using the CapPoly
 *   enforce rate limit specified in MaxCapRate
 *   enforce position limits specified in Min/MaxPosition
 *   apply FilterCap
 */
void cleanupCapADC (uint16_t adc)
{
    // previous cap position used for rate limiting
    static float prev_cap_nm;

    // convert to nm
    float cap_nm = ADC2nm ((float)adc);

    // detect and enforce legal rate limit
    float cap_nm_rate = 1E-3F * (cap_nm - prev_cap_nm) / loop_period_us;        // nm/usec -> m/sec
    if (cap_nm_rate > opcode_values[MaxCapRate_OC]) {
        // too fast positive, enforce legal rate but retain actual value read
        prev_cap_nm = cap_nm;
        cap_nm = prev_cap_nm + 1E3F * loop_period_us * opcode_values[MaxCapRate_OC];
        status_bits |= CapRateLimit_SBM;
    } else if (cap_nm_rate < -opcode_values[MaxCapRate_OC]) {
        // too fast negative, enforce legal rate but retain actual value read
        prev_cap_nm = cap_nm;
        cap_nm = prev_cap_nm - 1E3F * loop_period_us * opcode_values[MaxCapRate_OC];
        status_bits |= CapRateLimit_SBM;
    } else {
        prev_cap_nm = cap_nm;
    }

    // detect and enforce position limits
    if (cap_nm < opcode_values[MinPosition_OC]) {
        // too low
        cap_nm = opcode_values[MinPosition_OC];
        status_bits |= PositionReadTooLo_SBM;
    } else if (cap_nm > opcode_values[MaxPosition_OC]) {
        // too high
        cap_nm = opcode_values[MaxPosition_OC];
        status_bits |= PositionReadTooHi_SBM;
    }

    // filter
    cap_nm = runFIR7Filter (&c_filter, cap_nm);

    // inject fake value during testing
    if (opcode_values[StartTuning_OC] == 2) {
        static float fake_mp_value;
        if (fake_mp_value < opcode_values[MinPosition_OC]) {
            fake_mp_value = opcode_values[MinPosition_OC];
        } else if ((fake_mp_value += 1) > opcode_values[MaxPosition_OC]) {
            fake_mp_value = opcode_values[MinPosition_OC];
        }
        cap_nm = fake_mp_value;
    }

    // done
    opcode_values[MeasuredPosition_OC] = cap_nm;
}
