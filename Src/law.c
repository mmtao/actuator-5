/* run the next iteration of the Position loop or the simpler linear Coil current loop.
 */

#include "actuator.h"


#define KG_SCALE        (1E-6F)         // overall gain factor for nice O(1) values
#define	KI_CLAMP	(2/KG_SCALE)	// integral error windup limit


/* persistent values
 */
static float i_sum;			// integrator sum
static float prev_fderror;		// previous filtered error for differentiator
static float prev_mp;		        // previous measured capacitor position for velocity damping
static float ff_smoothed;               // FeedForward_OC smoothed with FFSmoothing_OC


/* initialize the persistent PID variables to begin closed-loop operation.
 */
void initPositionState()
{
    // handy
    float mp = opcode_values[MeasuredPosition_OC];
    float cc = opcode_values[CommandedCoil_OC];
    float og = opcode_values[OutputGain_OC];
    float ff = opcode_values[FeedForward_OC];

    // init differential filter to zero
    initFIR4Filter (&d_filter, 0.0F);
    prev_fderror = 0.0F;

    // preset the integrator gain so the initial PID output will match the commanded coil.
    i_sum = og != 0 ? cc / (og * KG_SCALE) : 0;

    // init error no change
    prev_mp = mp;

    // init smoothed ff to current value
    ff_smoothed = ff;
}

/* run next iteration of the Position control law.
 *
 * inputs:
 *    opcode_values[MeasuredPosition_OC] : filtered capacitor position, nm
 *    opcode_values[FeedForward_OC]	 : commanded feed forward, [-1,1]
 *    opcode_values[RunPosition_OC]	 : commanded position setpoint, nm
 *    opcode_values[ProportionalGain_OC] : proportional gain
 *    opcode_values[IntegralGain_OC]     : integral gain
 *    opcode_values[DifferentialGain_OC] : differential gain
 *    opcode_values[VelDampGain_OC]      : velocity damping gain
 *    opcode_values[OutputGain_OC]       : output gain
 *    loop_period_us                     : this loop period, microseconds
 *    prev_fderror		         : previous filtered error for differentiator
 *    prev_mp		                 : previous measured capacitor position for velocity damping
 *    d_filter                           : differentiator filter
 * output:
 *    opcode_values[CommandedCoil]	 : commanded coil value, [-1,1]
 *    status_bits			 : possible updates as required
 *
 * N.B. here we only _set_ CommandedCoil, it is _engaged_ elsewhere just after the cap is read
 */
void updatePositionState ()
{
    // handy measured position
    float mp = opcode_values[MeasuredPosition_OC];

    // error is the corrective action required. Since positive coil pulls the shell closer
    // and decreases the gap, if measured position is too large, we want to increase coil current
    float error = mp - opcode_values[RunPosition_OC];

    // accumulate (integrate) error over loop time, beware large windup
    i_sum += opcode_values[IntegralGain_OC] * error * loop_period_us;
    if (i_sum < -KI_CLAMP) {
        i_sum = -KI_CLAMP;
    } else if (i_sum > KI_CLAMP) {
        i_sum = KI_CLAMP;
    }

    // filter the error signal for use by differentiator
    float fderror = runFIR4Filter (&d_filter, error);

    // find rate of change in filtered error over loop time
    float de = (fderror - prev_fderror) / loop_period_us;

    // sum the PID terms
    float output = opcode_values[ProportionalGain_OC] * error
                   + i_sum
                   + opcode_values[DifferentialGain_OC] * de;

    // add vel damping
    output += opcode_values[VelDampGain_OC] * (mp - prev_mp) / loop_period_us;

    // apply final output gain, accounting covertly for tens of thousands cap value to unity coil current
    output *= opcode_values[OutputGain_OC] * KG_SCALE;

    // add smoothed feed-forward value
    float ff_s = opcode_values[FFSmoothing_OC];
    ff_smoothed = (1-ff_s)*opcode_values[FeedForward_OC] + ff_s*ff_smoothed;
    output += ff_smoothed;

    // clamp [-1,1] and note
    if (output < -1.0F) {
        output = -1.0F;
        status_bits |= CoilTooLo_SBM;
    } else if (output > 1.0F) {
        output = 1.0F;
        status_bits |= CoilTooHi_SBM;
    }

    // store output final command coil value
    opcode_values[CommandedCoil_OC] = output;

    // save state for next iteration
    prev_fderror = fderror;
    prev_mp = mp;
}

/* run next iteration of the Coil control law.
 *
 * inputs:
 *    opcode_values[RunCoil]		: target commanded current value, [-1,1]
 *    opcode_values[CoilRampPeriod]	: period to change coil full range
 *    opcode_values[CommandedCoil]	: previous commanded coil value
 * output:
 *    opcode_values[CommandedCoil]	: new commanded coil value
 *
 * N.B. here we only set opcode_values[CommandedCoil], it is engaged just after the cap is read
 */
void updateCoilState ()
{
    // compute total required change
    float change = opcode_values[RunCoil_OC] - opcode_values[CommandedCoil_OC];

    // compute maximum change allowed per loop period over full range [-1,1]
    float max_change = 2.0e-6F*loop_period_us/opcode_values[CoilRampPeriod_OC]; // convert per s to per us

    // nail it if rate is acceptable, else move towards goal at prescribed rate
    if (change > max_change) {
        opcode_values[CommandedCoil_OC] += max_change;
    } else if (change < -max_change) {
        opcode_values[CommandedCoil_OC] -= max_change;
    } else {
        opcode_values[CommandedCoil_OC] = opcode_values[RunCoil_OC];
    }
}
