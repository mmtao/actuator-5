/* code to read the chip temperature.
 */

#include "actuator.h"

// see App Note AN3964 and any STM32F4 data sheet
#define TS_CAL1         (*((uint16_t*)(0x1FFF7A2C)))                    // raw 30 C calibration
#define TS_CAL2         (*((uint16_t*)(0x1FFF7A2E)))                    // raw 110 C calibration

// ADC DMA target memory location
static uint16_t dma_temp;

/* initialize temperature reading system
 */
void initTemperature()
{
    // start continuous DMA
    HAL_ADC_Start_DMA (&hadc1, (uint32_t*)&dma_temp, 1);                // n half-words
}

/* return the current chip temperature in degrees C.
 */
float readTemperature()
{
    uint16_t volts = dma_temp;                                          // 12 bits each 3.3/4096 volts
    float degC;

    if (volts == 0) {
        status_bits |= TempReadErr_SBM;
        degC = -999.0F;
    } else {
        degC = 80.0F/(TS_CAL2-TS_CAL1)*(volts-TS_CAL1) + 30.0F;       // linear interpolation
        if (degC < -30 || degC > 120) {
            status_bits |= TempReadErr_SBM;
        }
    }

    // printf ("%u %u %u %g\n", volts, TS_CAL1, TS_CAL2, degC);
    return (degC);
}
