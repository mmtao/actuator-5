/* misc port and bit tools
 */

#include "actuator.h"

void uSec_Delay (int usecs)
{
    uint32_t t0 = getClock();
    uint32_t ticks = usecs*MHz;
    if (ticks > 20) {           // account for calling overhead
        ticks -= 20;
    }
    while (getClock() - t0 < ticks) {
        continue;
    }
}

void nSec_Delay (int nsecs)
{
    uint32_t t0 = getClock();
    uint32_t ticks = nsecs*MHz/1000;
    if (ticks > 20) {           // account for calling overhead
        ticks -= 20;
    }
    while (getClock() - t0 < ticks) {
        continue;
    }
}

/* convert HAL GPIO_Pin bitmask into ordinal from 0.
 * eg GPIO_Pin_3 is 0x08 so we return 3.
 */
uint16_t pinOrdinal (uint16_t pin)
{
    for (uint16_t i = 0; i < 16; i++)
        if (pin & (1<<i)) {
            return (i);
        }
    return (16);

}

void setPinAsInput (GPIO_TypeDef *port, uint16_t pin)
{
    port->MODER &= (~0UL & ~(3U << (2*pinOrdinal(pin))));
}

void setPinAsOutput (GPIO_TypeDef *port, uint16_t pin)
{
    uint16_t ord2x = 2*pinOrdinal(pin);
    uint32_t m = port->MODER;
    m &= (~0UL & ~(3U << (ord2x)));
    m |= (1UL << (ord2x));
    port->MODER = m;
}

void Set_Port (GPIO_TypeDef *port, uint16_t pin, bool value)
{
    FAST_WR_PIN (port, pin, value ? GPIO_PIN_SET : GPIO_PIN_RESET);

}

bool Get_Port (GPIO_TypeDef *port, uint16_t pin)
{
    return (FAST_RD_PIN (port, pin) ? GPIO_PIN_SET : GPIO_PIN_RESET);

}

