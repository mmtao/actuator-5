# Actuator Design Notes.

## Software.

The microcontroller in the design has 128KB of RAM and 512KB of flash.

The flash is divided as in this diagram:
![Flash memory map diagram](stm32f446-flash.png)

Upon reset, the microcontroller fetches
two words from the bottom of the flash:
the address of the initial stack pointer
(which will near the top of SRAM), and
the address of the next instruction to
execute (the address of `Reset_Handler` in
the current software).

### Design.

The software for the actuators is written in C using the STM-supplied hardware-abstraction layer (HAL).

There are design elements that limit the software's maturity and performance.
* _Reprogramming_. There is only one copy of the software in flash. The risk of bricking
one or more actuators makes developing the software further nearly impossible. 
* _Priority inversion_. The software does not use an operating system, so low-priority,
long-lived tasks (e.g., responding to messages) can delay higher priority, control tasks.
* _CMSIS-DSP and LZ4_. The software does not use freely available code that would simplify
and speed up the system. For example, the PID control loop is implemented manually, rather 
than with the CMSIS-DSP software freely available from ARM. Also, the image that the
microconroller sends to the FPGA is stored uncompressed, but it is highly compressible with
the freely-available LZ4. In its default mode, LZ4 compresses the 71399 bytes in iCE40.bin
to 6404 bytes; in the highest compression mode, 5005 bytes.

### Suggestions.

1. Use LZ4 to compress the iCE40 image and decompress it to RAM before sending it.
2. Use the CMSIS-DSP implementation of the PID control loop.
3. Use the free Zephyr operating system that supports this microcontroller.
4. Create a bootloader only build of software that is never reprogrammed and only knows 
   how to use the serial interface. It will boot and wait for messages. If no messages are
   received in the idle time (e.g., 2 seconds), the bootloader attempts to boot the 
   second software image. This means that failed reprogramming events are recoverable; when
   the bootloader _does_ receive a message in the idle time, it waits to be reprogrammed.

## Electronics.

Here is a summary of the electronic design of the acutators.

![Simplified diagram of the actuator electronics](electronics.png)

Partial BOM:
* [TLV-316: Single, 5.5-V, 10-MHz, RRIO operational amplifier.](https://www.ti.com/product/TLV316) ~$0.50.
* [AD7980: 16-Bit, 1 MSPS, PulSAR ADC in MSOP/LFCSP.](https://www.analog.com/en/products/ad7980.html) ~$12.
* [iCE5LP-4K: Small Footprint, Low Power FPGA.](https://www.latticesemi.com/Products/FPGAandCPLD/iCE40Ultra) ~$6.
* [STM32F446: Arm Cortex-M4 core with DSP and FPU, 512 Kbytes of Flash memory, 180 MHz CPU.](https://www.st.com/en/microcontrollers-microprocessors/stm32f446ze.html) ~$10.
* [DRV8838: 11-V, 1.8-A H-bridge motor driver with phase/enable control.](https://www.ti.com/product/DRV8838) ~$1.50.
* [INA240A4: -4 to 80V, bidirectional current sense amplifier.](https://www.ti.com/product/INA240) ~$2.
* [DS90LV011A: LVDS single high-speed differential driver.](https://www.ti.com/product/DS90LV011A) ~$1.25.
* [DS90LV028A: LVDS dual high speed differential receiver.](https://www.ti.com/product/DS90LV028A) $1.50.
* [LT1761: 100mA, Low Noise, LDO Micropower Regulator.](https://www.analog.com/en/products/lt1761.html) For 1.2, 2.5, 3.3V power planes. $2.50 each.
* [LT6657: 1.5ppm/°C Drift, Low Noise, Buffered Reference.](https://www.analog.com/en/products/lt6657.html) For 5V reference. ~$10.
* [DS2411: Silicon serial number. ](https://www.analog.com/en/products/ds2411.html) $2.

If one were to design a new actuator, there are multiple opportunities to reduce the cost.
Replacing the AD7980, iCE5LP, STM32F446, and DS2411 with a single [TI F280034](https://www.ti.com/product/TMS320F280034) 
would potentially greatly reduce the complexity of the system and the price of the BOM. The TI microcontroller requires only
a single 3.3V supply, instead of 1.2, 2.5, and 3.3. This would require verifying that (1) the 12-bit ADC on the TI part can be 
oversampled to get the 16-bit accuracy of the AD part; (2) that the slower core plus the second core is fast enough to do the
required math. This would, however, replace approximately $33 worth of parts for $3.