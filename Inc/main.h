/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H__
#define __MAIN_H__

/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define SPI_MISO_Pin GPIO_PIN_4
#define SPI_MISO_GPIO_Port GPIOB
#define SPI_SCK_Pin GPIO_PIN_3
#define SPI_SCK_GPIO_Port GPIOB
#define SWCLK_Pin GPIO_PIN_14
#define SWCLK_GPIO_Port GPIOA
#define SWDIO_Pin GPIO_PIN_13
#define SWDIO_GPIO_Port GPIOA
#define SPI_MOSI_Pin GPIO_PIN_5
#define SPI_MOSI_GPIO_Port GPIOB
#define UART_A2H_TX_Pin GPIO_PIN_10
#define UART_A2H_TX_GPIO_Port GPIOC
#define CRESET_Pin GPIO_PIN_12
#define CRESET_GPIO_Port GPIOA
#define IO_1_Pin GPIO_PIN_0
#define IO_1_GPIO_Port GPIOF
#define IO_2_Pin GPIO_PIN_1
#define IO_2_GPIO_Port GPIOF
#define UART_H2A_TX_UNUSED_Pin GPIO_PIN_12
#define UART_H2A_TX_UNUSED_GPIO_Port GPIOC
#define CDONE_Pin GPIO_PIN_11
#define CDONE_GPIO_Port GPIOA
#define OSC_IN_Pin GPIO_PIN_0
#define OSC_IN_GPIO_Port GPIOH
#define IO_3_Pin GPIO_PIN_2
#define IO_3_GPIO_Port GPIOF
#define LED_Pin GPIO_PIN_7
#define LED_GPIO_Port GPIOB
#define OSC_OUT_UNUSED_Pin GPIO_PIN_1
#define OSC_OUT_UNUSED_GPIO_Port GPIOH
#define IO_4_Pin GPIO_PIN_3
#define IO_4_GPIO_Port GPIOF
#define IO_5_Pin GPIO_PIN_4
#define IO_5_GPIO_Port GPIOF
#define IO_6_Pin GPIO_PIN_5
#define IO_6_GPIO_Port GPIOF
#define UART_H2A_RX_Pin GPIO_PIN_2
#define UART_H2A_RX_GPIO_Port GPIOD
#define IO_8_Pin GPIO_PIN_7
#define IO_8_GPIO_Port GPIOF
#define IO_7_Pin GPIO_PIN_6
#define IO_7_GPIO_Port GPIOF
#define IO_14_Pin GPIO_PIN_6
#define IO_14_GPIO_Port GPIOG
#define IO_13_Pin GPIO_PIN_5
#define IO_13_GPIO_Port GPIOG
#define PWM_OUT_Pin GPIO_PIN_2
#define PWM_OUT_GPIO_Port GPIOB
#define IO_10_Pin GPIO_PIN_1
#define IO_10_GPIO_Port GPIOG
#define IO_12_Pin GPIO_PIN_3
#define IO_12_GPIO_Port GPIOG
#define IO_11_Pin GPIO_PIN_2
#define IO_11_GPIO_Port GPIOG
#define UART_A2H_RX_UNUSED_Pin GPIO_PIN_1
#define UART_A2H_RX_UNUSED_GPIO_Port GPIOA
#define COIL_ADC_IN_Pin GPIO_PIN_5
#define COIL_ADC_IN_GPIO_Port GPIOA
#define STLK_RX_UNUSED_Pin GPIO_PIN_5
#define STLK_RX_UNUSED_GPIO_Port GPIOC
#define IO_9_Pin GPIO_PIN_0
#define IO_9_GPIO_Port GPIOG
#define GO_IN_Pin GPIO_PIN_0
#define GO_IN_GPIO_Port GPIOB
#define GO_IN_EXTI_IRQn EXTI0_IRQn
#define DALLAS_SN_Pin GPIO_PIN_3
#define DALLAS_SN_GPIO_Port GPIOA
#define PWM_DIR_Pin GPIO_PIN_1
#define PWM_DIR_GPIO_Port GPIOB
#define STLK_TX_Pin GPIO_PIN_10
#define STLK_TX_GPIO_Port GPIOB

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
/* #define USE_FULL_ASSERT    1U */

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
