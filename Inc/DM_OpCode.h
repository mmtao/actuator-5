#ifndef _DM_OPCODE_H
#define _DM_OPCODE_H

/* N.B. Do not make any external references here. This file is used both in C++ linux applications and
 * in stand-alone C embedded code.
 */

/* CASC opcodes for communication between Actuator, Central Hub (CH) and Reconstruction Computer (RC)
 */

/* symbolic opcodes enumeration
 */
typedef enum {

  /* The following opcodes are sent by RC and returned in lieu of next StatusBits message as an
   * acknowledgement.
   */
  ProportionalGain_OC = 0,  //   0 PID loop Proportional gain
  IntegralGain_OC,          //   1 PID loop Integral gain
  DifferentialGain_OC,      //   2 PID loop Derivative gain
  VelDampGain_OC,           //   3 Velocity damping gain
  OutputGain_OC,            //   4 Output gain

  CoilRampPeriod_OC,  //   5 Time to make a full range [-1,1] coil change, seconds
  MinPosition_OC,     //   6 Minimum acceptable measured position, nm
  MaxPosition_OC,     //   7 Maximum acceptable measured position, nm
  MaxCapRate_OC,      //   8 Max allowed measured position rate, nm/us

  FilterCapZ0_OC,  //   9 Capacitor FIR filter coefficient 0
  FilterCapZ1_OC,  //  10 Capacitor FIR filter coefficient 1
  FilterCapZ2_OC,  //  11 Capacitor FIR filter coefficient 2
  FilterCapZ3_OC,  //  12 Capacitor FIR filter coefficient 3
  FilterCapZ4_OC,  //  13 Capacitor FIR filter coefficient 4
  FilterCapZ5_OC,  //  14 Capacitor FIR filter coefficient 5
  FilterCapZ6_OC,  //  15 Capacitor FIR filter coefficient 6

  FilterDiffZ0_OC,  //  16 Differentiation FIR filter coefficient 0
  FilterDiffZ1_OC,  //  17 Differentiation FIR filter coefficient 1
  FilterDiffZ2_OC,  //  18 Differentiation FIR filter coefficient 2
  FilterDiffZ3_OC,  //  19 Differentiation FIR filter coefficient 3

  CapCalGain_OC,    //  20 cap ADC to nm gain coefficient
  CapCalOffset_OC,  //  21 cap ADC to nm ADC offset
  CapCalVg_OC,      //  22 cap ADC to nm polynominal Go voltage
  LEDBlinkRate_OC,  //  23 LED blink rate, Hz; 0 for off

  WildCoilDt_OC,   //  24 Period during which wild swings in MeasuredCoil are detected, seconds
  FFSmoothing_OC,  //  25 FF exponential filter coefficient, fraction of history to use, 0-1

  SerialNumber_OC,  //  26 Returns unique serial number, payload ignored
  SWVersion_OC,     //  27 Returns software version number, payload ignored

  // opcodes from here on are NOT preserved in the .acf file format. see N_FILE_SAVE_OC

  Temperature_OC,  //  28 Returns actuator temperature, C, payload ignored
  StartTuning_OC,  //  29 non-0 to engage Tuning state, else 0 to turn off

  CommandedCoil_OC,  //  30 Returns commanded coil [-1,1], payload ignored
  RawCapADC_OC,      //  31 Raw cap ADC value
  GetOpCode_OC,      //  32 Payload is Opcode value to be returned in lieu of next StatusBits
  Uptime_OC,         //  33 Returns seconds since boot, payload ignored
  EStop_OC,          //  34 command immediate safe current, payload echoed but ignored

  ProgramAddr_OC,     //  35 Set Actuator program address for next ProgramDataInc
  ProgramDataInc_OC,  //  36 Stage the given program value and increment program address
  ProgramFLASH_OC,    //  37 Commit new staged program to FLASH and reboot; payload ignored

  /* The following opcodes are used for immediate commands; they do not effect subsequent StatusBits
   */
  RunPosition_OC,  //  38 Set Commanded position, nm, and change to or resume Position state
  FeedForward_OC,  //  39 Feed-forward current, [-1,1], required just before RunPosition
  RunCoil_OC,      //  40 Set Commanded coil current, [-1,1]; change to or continue Coil state
  NoOp_OC,         //  41 Do nothing; useful to prevent HBTimeout

  /* The following opcodes are sent periodically by Actuators to RC automatically.
   */
  MeasuredPosition_OC,  //  42 Filtered position derived from capacitor, nm
  MeasuredCoil_OC,      //  43 Measured coil current, [-1,1]
  StatusBits_OC,        //  44 Actuator status information, see below
  TuningData_OC,        //  45 cap position low word, commanded coil hi word, both little-endian

  /* Total number of opcodes used by Actuators
   */
  N_ACTUATOR_OC,

  /* The following opcodes from RC are acted on directly by the CH, they are not forwarded to Actuators.
   * N.B. these must all be numerically >= 128
   */
  CH_OpCodeActNum_OC = 128,  // 128 Inform CH which Actuator's opcode to use for RC packets; default 0
  CH_TuningActNum_OC,        // 129 Inform CH which Actuator may be used for tuning msgs; default 0
  CH_Reset_OC,               // 130 Reset as if power cycled
  CH_SetTestCap_OC,          // 131 Set capacitor in test fixture, not used by production DM
  CH_SetGoLevel_OC,          // 132 Set Go level to progressively lower values, ie, 0 is max
  CH_TuningDataOpCode_OC,    // 133 Inform CH the value of TuningData_OC

  /* The following opcodes from CH are used when the Actuator designated by CH_OpCodeActNum_OC is not
   * functioning.
   */
  CH_NoOpCodeAct_OC = 254,  // 254 No message received; Actuator payloads are undefined
  CH_BadOpCodeAct_OC,       // 255 Bungled message received; Actuator payloads are undefined

  /* total number of potential opcodes
   */
  N_ALL_OC

} DM_OpCode;

/* The following define the bits in one StatusBits message.
 */
typedef enum {

  PositionElseCoil_SB = 0,  // Set if in Position state, else Coil state
  TuningActive_SB,          // Set if Tuning mode is active, else not
  RunPosNotReady_SB,        // Set if received RunPosition before all prerequisites were set
  RunCoilNotReady_SB,       // Set if received RunCoil before all prerequisites were set
  HBTimeout_SB,             // Set if no messages at all received for TBD

  BadChecksum_SB,        // Set if any CH messages since last report had a bad checksum
  BadOpCode_SB,          // Set if any CH messages since last report contained unknown OpCode
  CoilTooHi_SB,          // Set if any Position loop generated a commanded coil current > 1
  CoilTooLo_SB,          // Set if any Position loop generated a commanded coil current < -1
  PositionReadTooHi_SB,  // Set if any read positions since last report were > MaxPosition

  PositionReadTooLo_SB,  // Set if any read positions since last report were < MinPosition
  PositionCmdTooHi_SB,   // Set if any commanded positions since last report were > MaxPosition
  PositionCmdTooLo_SB,   // Set if any commanded positions since last report were < MinPosition
  CoilCmdTooHi_SB,       // Set if any commanded coil currents since last report were > 1
  CoilCmdTooLo_SB,       // Set if any commanded coil currents since last report were < -1

  CapRateLimit_SB,  // Set if cap change ever exceeded the rate limit since last report
  CapReadErr_SB,    // Set if capacitor ADC ever failed since last report
  TimingError_SB,   // Set if inner loop is taking longer than one Go clock period
  LEDIsOn_SB,       // Set if the LED is currently illuminated
  WildCoil_SB,      // Set if MeasuredCoil swings too much within WildCoilDt

  TempReadErr_SB,      // Set if temperature ADC ever failed since last report
  PosWithoutFF_SB,     // Set if RunPosition was received without a preceeding FeedForward
  FLASHError_SB,       // Set if error writing new program to µP FLASH
  SerialNumberErr_SB,  // Set if unable to read Serial number
  NoGo_SB,             // Set if no rising Go clock edge is detected after 10 µs

  FPGALoadError_SB,  // Set if unable to load FPGA
  N_STATUSBITS_SB

} DM_OpCode_StatusBits;

// handy StatusBits as bit mask
#define PositionElseCoil_SBM (1 << PositionElseCoil_SB)    // 0x00000001
#define TuningActive_SBM (1 << TuningActive_SB)            // 0x00000002
#define RunPosNotReady_SBM (1 << RunPosNotReady_SB)        // 0x00000004
#define RunCoilNotReady_SBM (1 << RunCoilNotReady_SB)      // 0x00000008
#define HBTimeout_SBM (1 << HBTimeout_SB)                  // 0x00000010
#define BadChecksum_SBM (1 << BadChecksum_SB)              // 0x00000020
#define BadOpCode_SBM (1 << BadOpCode_SB)                  // 0x00000040
#define CoilTooHi_SBM (1 << CoilTooHi_SB)                  // 0x00000080
#define CoilTooLo_SBM (1 << CoilTooLo_SB)                  // 0x00000100
#define PositionReadTooHi_SBM (1 << PositionReadTooHi_SB)  // 0x00000200
#define PositionReadTooLo_SBM (1 << PositionReadTooLo_SB)  // 0x00000400
#define PositionCmdTooHi_SBM (1 << PositionCmdTooHi_SB)    // 0x00000800
#define PositionCmdTooLo_SBM (1 << PositionCmdTooLo_SB)    // 0x00001000
#define CoilCmdTooHi_SBM (1 << CoilCmdTooHi_SB)            // 0x00002000
#define CoilCmdTooLo_SBM (1 << CoilCmdTooLo_SB)            // 0x00004000
#define CapRateLimit_SBM (1 << CapRateLimit_SB)            // 0x00008000
#define CapReadErr_SBM (1 << CapReadErr_SB)                // 0x00010000
#define TimingError_SBM (1 << TimingError_SB)              // 0x00020000
#define LEDIsOn_SBM (1 << LEDIsOn_SB)                      // 0x00040000
#define WildCoil_SBM (1 << WildCoil_SB)                    // 0x00080000
#define TempReadErr_SBM (1 << TempReadErr_SB)              // 0x00100000
#define PosWithoutFF_SBM (1 << PosWithoutFF_SB)            // 0x00200000
#define FLASHError_SBM (1 << FLASHError_SB)                // 0x00400000
#define SerialNumberErr_SBM (1 << SerialNumberErr_SB)      // 0x00800000
#define NoGo_SBM (1 << NoGo_SB)                            // 0x01000000
#define FPGALoadError_SBM (1 << FPGALoadError_SB)          // 0x02000000

// Mask of StatusBits that get reset each time they are sent a2h in StatusBits, the others persist
#define DM_RESET_STATUS_BM                                                                                                                                                            \
  (BadChecksum_SBM | BadOpCode_SBM | CoilTooHi_SBM | CoilTooLo_SBM | PositionReadTooHi_SBM | PositionReadTooLo_SBM | PositionCmdTooHi_SBM | PositionCmdTooLo_SBM | CoilCmdTooHi_SBM | \
   CoilCmdTooLo_SBM | CapRateLimit_SBM | CapReadErr_SBM | TimingError_SBM | WildCoil_SBM | TempReadErr_SBM | PosWithoutFF_SBM | NoGo_SBM)

// Mask of StatusBits monitored by Health and Safety system
#define H_AND_S_STATUS_BM                                                                                                                                                                  \
  (HBTimeout_SBM | BadChecksum_SBM | BadOpCode_SBM | CoilTooHi_SBM | CoilTooLo_SBM | PositionReadTooHi_SBM | PositionReadTooLo_SBM | CapRateLimit_SBM | CapReadErr_SBM | TimingError_SBM | \
   WildCoil_SBM | TempReadErr_SBM | PosWithoutFF_SBM | NoGo_SBM)

// count of _OC preserved in .acf file format
#define N_FILE_SAVE_OC (SWVersion_OC + 1)

#endif  // _DM_OPCODE_H
