/* glue between files used in the Actuator project
 */

#ifndef _ACTUATOR_H
#define _ACTUATOR_H

// C includes
#include <stdbool.h> 
#include <string.h>
#include <stdint.h>
#include <math.h>

// STM includes
#include "stm32f4xx_hal.h"
#include "adc.h"
#include "dma.h"
#include "gpio.h"
#include "main.h"
#include "tim.h"
#include "usart.h"


// MMT project opcode constants
#include "DM_OpCode.h"

#define SW_VERSION      3.07            // software version number (float)
// 3.07  fix serial number: zero bits[] in serial.c
// 3.06  change from MaxCapDev to WildCoil test
// 3.05  fix i_sum preload to match V3.02 
// 3.04  try better Maxim serial number timing
// 3.03  remove Reserved*_OC; add FFSmoothing_OC; N.B. Program*_OC changed
// 3.02  change Ki to each error, not sum
// 3.01  change back to DEADDEAF, prefer something more obscure
// 3.00  4V supply, 12 MHz clock
// 2.20  change IGNORE_MSG from DEADDEAF to 9999 to match ACF value
// 2.19  swap LEDBlickRate and RawCapADC opcodes so only former is in config file
// 2.18  Change capcal back to Gain*Vg/(ADU+Offset)
// 2.17  rm cap spike filter
// 2.16  Skip cap read if rx or tx; fix sign of Kv
// 2.15  Change capcal to Gain*Vg/ADU+Offset; increase FC_PERIOD
// 2.14  Add RawCapADC_OC
// 2.13  sync LED to Go, not internal clock; clean up timing constants
// 2.12: new ADC -> NM capacitor calibration formula
// 2.11: maintain Coil mode for CAP_HOLD_S once CapDevValue_SBM is triggered
// 2.10: implement new CapDevValue_OC
// 2.09: init PID integrator so output matches coil
// 2.08: retry getting serial number
// 2.07: faster PWM start after interrupt
// 2.06: a bit more PWM
// 2.05: flip sign of H bridge control AND sign of coil
// 2.04: flip sign of coil
// 2.03: FPGA V2555: low>high guard; 4.2 us cnv timing; 30 us no-strobe timeout
// 2.02: always H2A, A2H and use cap only if no H2A; 30 us Tuning
// 2.01: working FPGA and serial number
// 2.00: first release for Rev 4 with FPGA to read cap
// 1.21: add MaxCapDeviation_OC; 1 second HB
// 1.20: change CapTable from spline to 3rd degree polynomial
// 1.19: ignore messages if payload is DEADDEAF
// 1.18: only reset receipt of FeedForward if get a different RunPos values
// 1.17: Ignore StartTuning other than 0-2; move cap pos limiter before filter
// 1.16: change Cap to 7 FIR
// 1.15: rm FF filter; change Cap to IIR; add SerialNumber
// 1.14: always read ADC twice, even if not used
// 1.13: use rising edge of cap SPI
// 1.12: coast if H2A message
// 1.11: 4-phase Tuning cycle
// 1.10: HW triggered ADC conversion
// 1.09: flip sign of PWM so Kg is positive
// 1.08: redefine CapTable
// 1.07: continuous DMA for temperature and coid ADCs
// 1.06: improve tuning mode rate
// 1.05: Reduce FC_VALUE
// 1.04: Change checksum to CCITT CRC-8
// 1.03: StartTuning value can be 2 to generate serial content for testing
// 1.02: 100 kHz Go once single cap read ok
// 1.01: 80 kHz Go for time to do double cap read
// 1.00: initial working release


/*******************************************************************************
 *
 * actuator.c
 *
 *******************************************************************************/

extern void initActuator(void);
extern void runActuator(void);



/*******************************************************************************
 *
 * bitbang.c
 *
 *******************************************************************************/

extern void uSec_Delay (int usecs);
extern void nSec_Delay (int nsecs);
extern uint16_t pinOrdinal (uint16_t pin);
extern void setPinAsInput (GPIO_TypeDef *port, uint16_t pin);
extern void setPinAsOutput (GPIO_TypeDef *port, uint16_t pin);
extern void Set_Port (GPIO_TypeDef *port, uint16_t pin, bool value);
extern bool Get_Port (GPIO_TypeDef *port, uint16_t pin);



/*******************************************************************************
 *
 * capacitor.c
 *
 *******************************************************************************/

extern void cleanupCapADC (uint16_t adc);



/*******************************************************************************
 *
 * checksum.c
 *
 *******************************************************************************/

extern void setChecksum (uint8_t msg[]);
extern bool checksumOk (uint8_t msg[]);



/*******************************************************************************
 *
 * coil.c
 *
 *******************************************************************************/

extern void commandCoil(void);
extern void readCoilCurrent(void);
extern void initCoilCurrent(void);
extern void forceSafeCoil(void);





/*******************************************************************************
 *
 * filter.c
 *
 *******************************************************************************/


// capture the coefficients and history values for a 4-tap FIR filter state.
typedef struct {
    float z0, z1, z2, z3;               // coefficients
    float history[3];                   // history n-1, n-2, n-3
} FIR4Filter;

// capture the coefficients and history values for a 7-tap FIR filter state.
typedef struct {
    float z0, z1, z2, z3, z4, z5, z6;   // coefficients
    float history[6];                   // history n-1 .. n-6
} FIR7Filter;

extern void initFIR4Filter (FIR4Filter *fp, float v0);
extern float runFIR4Filter (FIR4Filter *fp, float v);
extern void initFIR7Filter (FIR7Filter *fp, float v0);
extern float runFIR7Filter (FIR7Filter *fp, float v);



/*******************************************************************************
 *
 * fpga.c
 *
 *******************************************************************************/

extern void bootFPGA(void);
extern bool readFPGA(uint16_t *value);



/*******************************************************************************
 *
 * fpga_program.c
 *
 *******************************************************************************/

extern const uint8_t fpga_program[];
extern const uint32_t fpga_program_size;



/*******************************************************************************
 *
 * iap.c
 *
 *******************************************************************************/

extern bool iap_setAddress (uint32_t a);
extern bool iap_setNextProgram (uint32_t v);
extern bool iap_commit(void);



/*******************************************************************************
 *
 * law.c
 *
 *******************************************************************************/

extern void initPositionState (void);
extern void updatePositionState (void);
extern void updateCoilState (void);



/*******************************************************************************
 *
 * runopcode.c
 *
 *******************************************************************************/

extern bool run_H2A_OpCode(void);



/*******************************************************************************
 *
 * serial.c
 *
 *******************************************************************************/

extern void getSerialNumber(void);
extern void uSec_Delay (int usecs);
extern void nSec_Delay (int nsecs);




/*******************************************************************************
 *
 * stm32crc.c
 *
 *******************************************************************************/

extern uint32_t calcSTM32CRC (uint32_t *buffer, uint32_t count);



/*******************************************************************************
 *
 * temp.c
 *
 *******************************************************************************/

extern void initTemperature (void);
extern float readTemperature (void);




/*******************************************************************************
 *
 * main.c
 *
 *******************************************************************************/


// Mask of DM_OpCodes required to run in Position state (FeedForward handled separately)
// N.B. we assume none of these values are >= 32
#define	REQ_POS_CODES		( \
    (1 << ProportionalGain_OC)	| \
    (1 << IntegralGain_OC)	| \
    (1 << DifferentialGain_OC)	| \
    (1 << VelDampGain_OC)	| \
    (1 << OutputGain_OC)	| \
    (1 << MinPosition_OC)	| \
    (1 << MaxPosition_OC)	| \
    (1 << MaxCapRate_OC)	| \
    (1 << FilterCapZ0_OC)	| \
    (1 << FilterCapZ1_OC)	| \
    (1 << FilterCapZ2_OC)	| \
    (1 << FilterCapZ3_OC)	| \
    (1 << FilterCapZ4_OC)	| \
    (1 << FilterCapZ5_OC)	| \
    (1 << FilterCapZ6_OC)	| \
    (1 << FilterDiffZ0_OC)	| \
    (1 << FilterDiffZ1_OC)	| \
    (1 << FilterDiffZ2_OC)	| \
    (1 << FilterDiffZ3_OC)	| \
    (1 << CapCalGain_OC)	| \
    (1 << CapCalOffset_OC)	| \
    (1 << CapCalVg_OC)		| \
    (1 << WildCoilDt_OC)        | \
    (1 << FFSmoothing_OC)	)

// Mask of DM_OpCodes required to run in Coil state
#define	REQ_COIL_CODES		( \
    (1 << CoilRampPeriod_OC)	)

#define H2A_MSG_LEN     6               // n bytes in all incoming H2A messages
#define A2H_MSG_LEN     6               // n bytes in normal outgoing A2H messages (exception is tuning)
#define PAYLOAD_LEN     4               // n bytes in message payload
#define OPCODE_OFFSET   0               // n bytes into message where opcode is
#define PAYLOAD_OFFSET  1               // n bytes into message where payload begins
#define CHKSUM_OFFSET   5               // n bytes into message where checksum is

#define MHz             180UL           // clock ticks per usec
#define CT_PER_GO       1800UL          // clock ticks per Go

#define GO_PER_SEC      (1000000UL*MHz/CT_PER_GO)
#define CT_PER_SEC      (1000000UL*MHz)

#define	getClock()	(DWT->CYCCNT)	// debug timer used for running clock ticks
#define	CT2FUS(t)	((float)(t)/MHz)// clock tick interval to float microseconds

#define FC_VALUE        0.3F            // initial or HB timeout or WildCoil standby coil current [-1,1]
#define FC_PERIOD       5.0F            // initial coil ramp period, seconds
#define WILD_HOLD_CT    CT_PER_SEC      // reject RunPositions after WildCoil this long, ticks

extern DM_OpCode next_a2h_status;
extern float opcode_values[N_ACTUATOR_OC];
extern uint32_t status_bits;
extern uint32_t wildhold_t0;

extern uint8_t h2a_msg[H2A_MSG_LEN];
extern uint8_t a2h_msg[A2H_MSG_LEN];
extern uint8_t *a2h_db_toggle;

extern FIR7Filter c_filter;
extern FIR4Filter d_filter;

extern float loop_period_us;
extern bool new_run_pos;
extern uint32_t led_counter, led_count;

extern void GOInterrupt(void);

// fast GPIO pin macros -- originals are in stm32f4xx_hal_gpio.c
#define FAST_WR_PIN(x,p,s)      (x)->BSRR = ((s) != GPIO_PIN_RESET) ? (p) : ((uint32_t)(p) << 16U)
#define FAST_RD_PIN(x,p)        (((x)->IDR & (p)) != (uint32_t)GPIO_PIN_RESET) ? GPIO_PIN_SET : GPIO_PIN_RESET
#define FAST_TOGGLE_PIN(x,p)    (x)->ODR ^= (p)

#endif // _ACTUATOR_H
